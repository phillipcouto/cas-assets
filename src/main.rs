
extern crate clap;
use clap::{Arg, App};

extern crate crypto;
use crypto::digest::Digest;
use crypto::sha2::Sha256;

extern crate url;
use url::Url;

use std::collections::HashMap;
use std::env;
use std::io;
use std::io::{Cursor, Read, Write};
use std::fs::{self, File};
use std::path::{Path, PathBuf};

type AssetList = Vec<(String, String)>;
type AssetNew = (PathBuf, Box<Vec<u8>>);
type AssetCollection = HashMap<String, String>;

fn main() {
    let matches = App::new("cas-assets")
        .version("1.0")
        .author("Phillip Couto <phillip@couto.in>")
        .about("Scans assets directory and creates cas filenames")
        .arg(Arg::with_name("src")
            .short("s")
            .long("src")
            .value_name("path")
            .help("path to folder containing assets to process")
            .required(true)
            .multiple(true)
            .takes_value(true))
        .arg(Arg::with_name("dest")
            .help("Destination, can be a url or local path")
            .short("d")
            .long("dest")
            .required(true)
            .takes_value(true))
        .get_matches();

    let dest = matches.value_of("dest").unwrap_or("./cas");

    println!("Destination location {}", dest);

    let mut map: AssetCollection = HashMap::new();

    for p in matches.values_of("src").unwrap() {
        println!("scanning {:}", p);
        scan_dir(&mut map, Path::new(p), dest).unwrap();
    }

    for key in map.keys() {
        println!("{:?} - {:}", key, map.get(key).unwrap());
    }
}

fn scan_dir(map: &mut AssetCollection, dir: &Path, dest: &str) -> io::Result<()> {
    if dir.is_dir() {
        for entry in try!(fs::read_dir(dir)) {
            let entry = try!(entry);
            let path = entry.path();
            if path.is_dir() {
                try!(scan_dir(map, &path, dest));
            } else {
                let mut a = try!(new_asset(path.to_path_buf()));
                let ap = a.0.clone();
                let p = ap.as_path();
                let ext = p.extension();
                if ext.is_some() {
                    if ext.unwrap().to_str().unwrap() == "css" {
                        let new_body = try!(transform_css(map, &a, dest));
                        a.1 = new_body;
                    }
                }
                let path_str = p.to_str().unwrap();
                let name = name_with_hash(&a);
                try!(write_out(&a, name.as_str(), dest));
                map.insert(path_str.to_string(), name);
            }
        }
    }
    Ok(())
}

fn new_asset(path: PathBuf) -> io::Result<AssetNew> {
    let mut f = try!(File::open(path.as_path()));
    let metadata = try!(f.metadata());
    let mut buf: Vec<u8> = Vec::with_capacity(metadata.len() as usize);

    try!(f.read_to_end(&mut buf));
    let a: AssetNew = (path, Box::new(buf));
    Ok(a)
}

fn hash(a: &AssetNew) -> String {
    let mut hasher = Sha256::new();
    hasher.input(a.1.as_ref());
    hasher.result_str()
}

fn name_with_hash(a: &AssetNew) -> String {
    let h = hash(a);
    let mut ext_string = a.0.extension().unwrap().to_str().unwrap().to_string();
    ext_string.insert(0, '.');
    let ext = ext_string.as_str();
    let name = a.0.file_name().unwrap().to_str().unwrap();
    let clean_name = name.replace(ext, "");
    clean_name + "-" + h.as_str() + ext
}

fn transform_css(map: &mut AssetCollection, a: &AssetNew, dest: &str) -> io::Result<Box<Vec<u8>>> {
    let mut txt = String::new();
    let mut rdr = Cursor::new(a.1.as_ref());
    let p = a.0.as_path();
    try!(rdr.read_to_string(&mut txt));
    let mut new_body = String::with_capacity(txt.len());
    for line in txt.lines() {
        let mut idx = line.find("url(");
        let mut buf = line;
        let mut idx_end = 0;
        while idx.is_some() {
            let mut idx_num = idx.unwrap();
            idx_num += 4;
            for c in buf[..idx_num].chars() {
                new_body.push(c);
            }
            buf = buf[idx_num..].as_ref();
            idx_end = buf.find(")").unwrap();
            let res: &str = buf[1..idx_end - 1].as_ref();
            if !res.starts_with("http") && !res.starts_with("//") && !res.starts_with("data") {
                let base = p.parent().unwrap();
                let new_asset_path = Path::new(res);
                let combined = base.join(new_asset_path);
                if combined.exists() {
                    let cwd = env::current_dir().unwrap();
                    let clean_pathbuf = try!(combined.canonicalize());
                    let stripped = match clean_pathbuf.strip_prefix(&cwd) {
                        Ok(p) => p,
                        Err(_) => {
                            panic!("asset path outside working directory, {:}",
                                   combined.display())
                        }
                    };
                    let f = try!(new_asset(stripped.to_path_buf()));
                    let name = name_with_hash(&f);
                    new_body.push('\'');
                    for c in name.chars() {
                        new_body.push(c);
                    }
                    buf = buf[idx_end - 1..].as_ref();
                    try!(write_out(&f, name.as_str(), dest));
                    map.insert(stripped.to_str().unwrap().to_string(), name);
                } else {
                    println!("skipping asset {:}, does not exist", combined.display());
                }
            }
            idx = buf.find("url(");
            idx_end = 0
        }
        for c in buf[idx_end..].chars() {
            new_body.push(c);
        }
        new_body.push('\n');
    }
    Ok(Box::new(new_body.into_bytes()))
}

fn write_out(a: &AssetNew, name: &str, dest: &str) -> io::Result<()> {
    let url_parse = Url::parse(dest);
    if url_parse.is_ok() {
        // Handle URL destination like S3 or Swift
    } else {
        // local file
        try!(write_out_file(a, name, dest));
    }

    Ok(())
}

fn write_out_file(a: &AssetNew, name: &str, dest: &str) -> io::Result<()> {
    let p = Path::new(dest);

    if !p.exists() {
        panic!("destination path does not exist");
    }

    let pb = p.join(Path::new(name));
    if pb.exists() {
        println!("skipping asset {:?}, already exists", name);
    } else {
        let mut f = try!(File::create(pb.as_path()));
        try!(f.write_all(a.1.as_ref()));
    }

    Ok(())
}
